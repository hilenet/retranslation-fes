require 'bundler/setup'
Bundler.require
require 'sinatra/reloader'
require './models/translate.rb'

##
enable :sessions

# 最新の結果を表示
get '/' do
	if Translate.count == 0
		Translate.create([
			{before: "retranslation", after: "再翻訳", lang: "ja"},
			{before: "再翻訳", after: "retranslation", lang: "af"},
			{before: "retranslation", after: "再変換", lang: "ja"}])
	end
	results = Translate.order(id: :desc)
	results.each do |r|
		if (r.lang == "ja") && (r.id != Translate.maximum('id'))
			@idNum = r.id
			break
		end 
	end
	@newResult = Translate.where("id > ?", @idNum) 
	
	##
	session[:lang] ||= "nothing"
	@languages=session[:lang]
	erb :index
end

# 再翻訳
post '/new' do
	p params
	before = params[:body].encode("UTF-8")
	lang = params[:lang]
	
	##
	session[:lang] = lang
	
	if lang == nil # エラー処理
		redirect '/'
	end
	n = 0
	while n <= lang.length
		if n > 0 
			before = @after
		end
		if n == lang.length
			language = "ja"
		else
			language = lang[n]
		end
		url = "https://www.googleapis.com/language/translate/v2?key=AIzaSyCwWcA0axOFGHB5rdHbeEspLuZXPnMwrvA&target=#{language}&q=#{before}"
		uri = URI.parse(URI.escape(url))
		json = Net::HTTP.get(uri)
		result = JSON.parse(json)
		@after =  result["data"]["translations"][0]["translatedText"]
		Translate.create(
			:before => before,
			:after => @after,
			:lang => language)
		n = n + 1
	end
	redirect '/'
end

# 履歴表示
get '/history' do
	history = Translate.all
	i = 1
	@his = []
	@time = []
	while i < history.length
		his2 = []
		until history[i].lang == "ja"
			if history[i-1].lang == "ja"
				his2.push(history[i].before)
			end
			his2.push(history[i].after)
			i = i + 1
		end
		his2.push(history[i].after)
		his2.push(history[i].created_at.localtime("+09:00").strftime("%c"))
		i = i + 1
		@his.push(his2)
	end
	puts "----history----"
	p @his
	erb :history
end

