class CreateTranslates < ActiveRecord::Migration
  def change
  	  	create_table :translates do |t|
  		t.string :before
  		t.string :after
  		t.string :lang
  		t.timestamps null: false
  	end
  end
end
